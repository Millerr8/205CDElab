// tasks.js
// This script manages a to-do list.

// Need a global variable:
var tasks = [];
var tasks2 = [];

// Function called when the form is submitted.
// Function adds a task to the global array.
function addTask() {
    'use strict';

    // Get the task:
    var task = document.getElementById('task');

    // Reference to where the output goes:
    var output = document.getElementById('output');

    // For the output:
    var message = '';

    if (task.value) {

        // Add the item to the array:
        tasks.push(task.value);

        // Update the page:
        message = '<h2>To-Do</h2><ol>';
        for (var i = 0, count = tasks.length; i < count; i++) {
            message += '<li>' + tasks[i] + '</li>';
        }
        message += '</ol>';
        output.innerHTML = message;

    } // End of task.value IF.

    // Return false to prevent submission:
    return false;

} // End of addTask() function.
function removeDupe(){
    'use strict';
    var output = document.getElementById('output')
    var message = '';
    
    // tasks2[0] = tasks[0];
    // for (var i = 0, count = tasks.length; i < count; i++){
    //     for (var j = 1, count2 = tasks.length; j< count2;j++){
    //         var value = tasks[i];
    //         var dupe = tasks[j]
    //         if (value != dupe){
    //             for (var q = 0, count3 =tasks2.length; q < count3;q++){
    //                 if (dupe in tasks2){
    //                     break;
    //                 }else{
    //                     tasks2.push(dupe)
    //                     break;
    //                 }
                    
    //             }
    //         }
        
    //     }
    // }
    
    for (var i=0; i<tasks.length;i++){
        var element = tasks[i];
        do{
            var index = tasks.indexOf(element);
            tasks[index] = null;
            index = tasks.indexOf(element);
        }while(index!=-1 && tasks[index] != null);
        if (element !=  null) {tasks2.push(element)}
    }
    message = '<h2>To-Do</h2><ol>';
        for (var f = 0, count4 = tasks2.length; f < count4; f++) {
            message += '<li>' + tasks2[f] + '</li>';
        }
        
        message += '</ol>';
        output.innerHTML = message;
}

// Initial setup:
function init() {
    'use strict';
    document.getElementById('theForm').onsubmit = addTask;
    document.getElementById('dupe').onclick = removeDupe;
} // End of init() function.
window.onload = init;